import React, { useEffect, useState } from "react";
import { FormikErrors, useFormik } from "formik";
import { Layout } from "../components/layout";
import useSWR from "swr";
import { CommunityDbModel } from "../models/Community";

interface CommunicationForm {
  question: string;
}
const fetcher = (url) => fetch(url).then((res) => res.json());

const Community = () => {
  const { data, mutate } = useSWR<CommunityDbModel[]>(
    "/api/333Community",
    fetcher
  );

  async function count(element) {
    const result = await fetch("api/333Community/count", {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({ _id: element._id }),
    });
    const response = await result.json();
    await mutate();
  }

  const [message, setMessage] = useState("");
  const [submitted, setSubmitted] = useState(false);
  const [messageClass, setMessageClass] = useState("");
  const formik = useFormik<CommunicationForm>({
    initialValues: {
      question: "",
    },

    onSubmit: async (values, { resetForm }) => {
      const result = await fetch("api/333Community/create", {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(values),
      });

      const response = await result.json();
      resetForm();
      setMessage(response.message);
      setSubmitted(true);
      setMessageClass("Success");
    },

    validate: (values) => {
      console.log(values);
      const errors = {} as FormikErrors<CommunicationForm>;
      if (values.question.length > 500) {
        errors.question = "Question Should be 500 Characters or Less";
      }
      return errors;
    },
  });
  useEffect(() => {
    console.log("effect", formik.values, submitted);
    if (submitted) {
      setMessageClass("");
      setSubmitted(false);
    }
  }, [formik.values]);
  return (
    <Layout>
      {data?.map((element) => {
        return (
          <div key={element._id}>
            <h2>{element.question}</h2>
            <button type="button" onClick={() => count(element)}>
              Useful
            </button>
            <p>{element.count} people found this useful</p>
          </div>
        );
      })}
      <form onSubmit={formik.handleSubmit}>
        <div>
          <div>
            <label htmlFor="question">Question</label>
          </div>
          <textarea
            className="communityInput"
            id="question"
            name="question"
            rows={4}
            onChange={formik.handleChange}
            value={formik.values.question}
          />

          {formik.errors.question}
        </div>
        <div className="onSubmit">
          <button type="submit">Submit</button>
          <div className={messageClass}>{message}</div>
        </div>
      </form>
    </Layout>
  );
};
export default Community;
