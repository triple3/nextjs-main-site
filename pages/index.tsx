import Head from "next/head";
import { Layout, siteTitle } from "../components/layout";
import { Pillar } from "../components/pillar";
import { SocialButton } from "../components/socialButton";
import styles from './index.module.scss';
export default function Home() {
  return (
    <Layout>
      <Head>
        <title>3rdHarmonicRecords</title>
      </Head>

      <div className="row flex-m-col flex-d-row">
        <div className="menuBar"></div>
        <Pillar
          heading="Who We Are"
          columnNumber={1}
          body="We are a HipHop/RnB Alternative Trap Record Label based in South Africa founded in 2021 and aimed at youth empowerment in the entertainment industry."
        ></Pillar>
        <Pillar
          columnNumber={2}
          heading="Mission"
          body="Our mission is to discover and seek out undiscovered talented young artists and provide them with the platform to prosper, nurtured by an environment of freedom and creativity."
        ></Pillar>
        <Pillar
          columnNumber={3}
          heading="Values"
          body="Transparency, Integrity and Quality."
        />
      </div>
      <div className={styles.links}>
        <SocialButton
          href="https://www.instagram.com/3rdharmonicrecords"
          socialNetworkType="instagram"
          text="3RD HARMONIC RECORDS"
        />
        <SocialButton
          href="https://www.instagram.com/dropoutfactory"
          socialNetworkType="instagram"
          text="DROP OUT FACTORY"
        />
        <SocialButton
          href="https://www.instagram.com/r_meo7230/"
          socialNetworkType="instagram"
          text="LIL ROLEY"
        />
        <SocialButton
          href="https://www.instagram.com/iamhat3trick"
          socialNetworkType="instagram"
          text="HAT3TRICK"
        />
      </div>
      <div className={styles.links}>
        <SocialButton
          href="https://twitter.com/the3rdharmonic"
          socialNetworkType="twitter"
          text="3RD HARMONIC RECORDS"
        />
        <SocialButton
          href="https://twitter.com/dofbfg"
          socialNetworkType="twitter"
          text="DROP OUT FACTORY"
        />
        <SocialButton
          href="https://twitter.com/hat3trick"
          socialNetworkType="twitter"
          text="HAT3TRICK"
        />
      </div>
    </Layout>
  );
}
