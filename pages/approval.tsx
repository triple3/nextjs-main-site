import { Layout } from "../components/layout";
import useSWR from "swr";
import { useState } from "react";

const fetcher = (url) => fetch(url).then((res) => res.json());

function Approval() {
  const { data, mutate, error } = useSWR("/api/333Community", fetcher);
  //   const [selectedQuestion, setSelectedQuestion] = useState(undefined);
  //   async function handleDelete() {
  //     console.log("deleted");
  //     await mutate();
  //     setSelectedQuestion(undefined);
  //   }
  async function del(element) {
    const result = await fetch("api/333Community/delete", {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({ _id: element._id }),
    });
    const response = await result.json();
    await mutate();
  }
  async function approve(element) {
    const result = await fetch("api/333Community/approve", {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({ _id: element._id }),
    });
    const response = await result.json();
    await mutate();
  }
  if (error) return <div>failed to load</div>;
  if (!data) return <div>loading...</div>;

  return (
    <Layout>
      {data.map((element) => {
        return (
          <div key={element._id}>
            <div>{element.question}</div>
            <div>{element.active ? "avtive" : "pending"}</div>
            <button type="button" onClick={() => del(element)}>
              Delete
            </button>
            <button type="button" onClick={() => approve(element)}>
              Approve
            </button>
          </div>
        );
      })}
    </Layout>
  );
}
export default Approval;
