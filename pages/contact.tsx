import React, { useEffect, useState } from "react";
import { FormikErrors, useFormik } from "formik";
import { Layout } from "../components/layout";
import { ContactDbModel, ContactDbModelSchema } from "../models/ContactModel";

const Contact = () => {
  const [message, setMessage] = useState("");
  const [submitted, setSubmitted] = useState(false);
  const [messageClass, setMessageClass] = useState("");
  const formik = useFormik<ContactDbModel>({
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      socialMedia: "",
    },
    onSubmit: async (values, { resetForm }) => {
      const result = await fetch("api/contact", {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(values),
      });
      const response = await result.json();
      resetForm();
      setMessage(response.message);
      setSubmitted(true);
      setMessageClass("Success");
    },

    validate: (values) => {
      // console.log(values);
      const errors = {} as FormikErrors<ContactDbModel>;
      if (values.firstName.length == 0) {
        errors.firstName = "First Name is Required";
      }
      return errors;
    },
  });
  useEffect(() => {
    console.log("effect", formik.values, submitted);
    if (submitted) {
      setMessageClass("");
      setSubmitted(false);
    }
  }, [formik.values]);
  return (
    <Layout>
      <form onSubmit={formik.handleSubmit}>
        <div>
          <div>
            <label htmlFor="firstName">First Name</label>
          </div>
          <div className=" ">
            <input
              id="firstName"
              name="firstName"
              type="text"
              onChange={formik.handleChange}
              value={formik.values.firstName}
            />
          </div>
          {formik.errors.firstName}
        </div>
        <div>
          <div>
            <label htmlFor="lastName">Last Name</label>
          </div>
          <input
            id="lastName"
            name="lastName"
            type="text"
            onChange={formik.handleChange}
            value={formik.values.lastName}
          />
        </div>
        <div>
          <div>
            <label htmlFor="email">Email Address</label>
          </div>
          <input
            id="email"
            name="email"
            type="email"
            onChange={formik.handleChange}
            value={formik.values.email}
          />
        </div>
        <div>
          <label htmlFor="socialMedia">Social Media Links</label>
        </div>
        <div>
          <input
            id="socialMedia"
            name="socialMedia"
            type="socialMedial"
            onChange={formik.handleChange}
            value={formik.values.socialMedia}
          />
        </div>
        <div className="onSubmit">
          <button className="onSubmit" type="submit">
            Submit
          </button>
          <div className={messageClass}>{message}</div>
        </div>
      </form>
    </Layout>
  );
};
export default Contact;
