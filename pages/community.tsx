import React, { useEffect, useState } from "react";
import { FormikErrors, useFormik } from "formik";
import { Layout } from "../components/layout";
import { FaqDbModel, FaqDbModelSchema } from "../models/FaqModel";
interface CommunicationForm {
  question: string;
}
const Faq = () => {
  const [message, setMessage] = useState("");
  const [submitted, setSubmitted] = useState(false);
  const [messageClass, setMessageClass] = useState("");
  const formik = useFormik<CommunicationForm>({
    initialValues: {
      question: "",
    },
    onSubmit: async (values, { resetForm }) => {
      const result = await fetch("api/community/create", {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(values),
      });

      const response = await result.json();
      resetForm();
      setMessage(response.message);
      setSubmitted(true);
      setMessageClass("Success");
    },

    validate: (values) => {
      console.log(values);
      const errors = {} as FormikErrors<CommunicationForm>;
      if (values.question.length > 500) {
        errors.question = "Question Should be 500 Characters or Less";
      }
      return errors;
    },
  });
  useEffect(() => {
    console.log("effect", formik.values, submitted);
    if (submitted) {
      setMessageClass("");
      setSubmitted(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [formik.values]);
  return (
    <Layout>
      <form onSubmit={formik.handleSubmit}>
        <div>
          <div>
            <label htmlFor="question">Question</label>
          </div>
          <textarea
            className="communityInput"
            id="question"
            name="question"
            rows={4}
            onChange={formik.handleChange}
            value={formik.values.question}
          />

          {formik.errors.question}
        </div>
        <div className="onSubmit">
          <button type="submit">Submit</button>
          <div className={messageClass}>{message}</div>
        </div>
      </form>
    </Layout>
  );
};
export default Faq;
