import { Layout } from "../components/layout";
import useSWR from "swr";
import { useState } from "react";
import { Faq } from "../components/Answer";

const fetcher = (url) => fetch(url).then((res) => res.json());

function Answer() {
  const { data, mutate, error } = useSWR("/api/community", fetcher);
  const [selectedQuestion, setSelectedQuestion] = useState(undefined);
  async function handleDelete() {
    console.log("deleted");
    await mutate();
    setSelectedQuestion(undefined);
  }
  if (error) return <div>failed to load</div>;
  if (!data) return <div>loading...</div>;

  return (
    <Layout>
      {data.map((element) => {
        return (
          <div key={element._id} onClick={() => setSelectedQuestion(element)}>
            {element.question}
          </div>
        );
      })}
      {selectedQuestion && (
        <Faq
          onDelete={() => {
            handleDelete();
          }}
          answerElement={selectedQuestion}
        />
      )}
    </Layout>
  );
}

export default Answer;
