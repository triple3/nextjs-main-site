import React, { useEffect, useState } from "react";
import { FormikErrors, useFormik } from "formik";
import { Layout } from "../components/layout";
import { FaqDbModel, FaqDbModelSchema } from "../models/FaqModel";
import useSWR from "swr";

const fetcher = (url) => fetch(url).then((res) => res.json());

const Faq = () => {
  const { data } = useSWR("/api/faq", fetcher);
  if (!data) return <div>loading...</div>;
  return (
    <Layout>
      <h2>FAQ</h2>
      {data.map((element) => {
        return (
          <div key={element._id}>
            <h2>{element.question}</h2>
            <p>{element.answer}</p>
          </div>
        );
      })}
    </Layout>
  );
};
export default Faq;
