import { NextApiRequest, NextApiResponse } from "next";
import { connectToDatabase, CommunityModel } from "../../../lib/mongodb";

export default async function del(req: NextApiRequest, res: NextApiResponse) {
  await connectToDatabase();
  if (req.method === "POST") {
    await CommunityModel.deleteOne({ _id: req.body._id });
    res.status(200).json({ message: "deleted" });
  }
  res.status(400);
}
