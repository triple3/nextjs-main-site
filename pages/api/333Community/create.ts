import { NextApiRequest, NextApiResponse } from "next";
import { connectToDatabase, CommunityModel, FaqModel } from "../../../lib/mongodb";

export default async function create(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method === "POST") {
    await connectToDatabase();
    FaqModel.validate(req.body);
    const com = new CommunityModel(req.body);
    await com.save();
    res.status(200).json({ message: "success" });
  } else {
    res.status(400);
  }
}
