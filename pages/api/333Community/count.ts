import { NextApiRequest, NextApiResponse } from "next";
import { connectToDatabase, CommunityModel } from "../../../lib/mongodb";

export default async function count(req: NextApiRequest, res: NextApiResponse) {
  await connectToDatabase();
  if (req.method === "POST") {
    const c = await CommunityModel.findById(req.body._id);
    c.count++;
    await c.save();
    res.status(200).json({ message: "Approved" });
  }
  res.status(400);
}
