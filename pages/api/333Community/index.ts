import type { NextApiRequest, NextApiResponse } from "next";
import { connectToDatabase, CommunityModel } from "../../../lib/mongodb";
export default async function index(req: NextApiRequest, res: NextApiResponse) {
  //await connectToDatabase();
  console.log(req.body, req.method);

  const data = await CommunityModel.find({});

  res.status(200).json(data);
}
