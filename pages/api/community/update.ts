import { NextApiRequest, NextApiResponse } from "next";
import { connectToDatabase, FaqModel } from "../../../lib/mongodb";

export default async function update(
  req: NextApiRequest,
  res: NextApiResponse
) {
  await connectToDatabase();
  if (req.method === "POST") {
    const c = await FaqModel.findById(req.body._id);
    c.question = req.body.question;
    c.answer = req.body.answer;
    c.active = req.body.active;
    await c.save();
    res.status(200).json({ message: "updated" });
  }
  res.status(400);
}
