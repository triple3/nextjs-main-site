import type { NextApiRequest, NextApiResponse } from "next";
import { connectToDatabase, FaqModel } from "../../../lib/mongodb";
export default async function index(req: NextApiRequest, res: NextApiResponse) {
  //await connectToDatabase();
  console.log(req.body, req.method);

  const data = await FaqModel.find({});

  res.status(200).json(data);
}
