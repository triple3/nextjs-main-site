import { NextApiRequest, NextApiResponse } from "next";
import { connectToDatabase, ContactModel } from "../../lib/mongodb";

export default async function create(
  req: NextApiRequest,
  res: NextApiResponse
) {

  if (req.method === "POST") {
    await connectToDatabase();
    ContactModel.validate(req.body);
    const com = new ContactModel(req.body);
    await com.save();
    res.status(200).json({ message: "success" })

  } else {
    res.status(400);
  }
}