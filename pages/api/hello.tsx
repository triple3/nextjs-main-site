const url = require('url');
export default function hello(req:Request, res) {
  const queryObject = url.parse(req.url,true).query;
  console.log(queryObject);
  res.status(200).json(queryObject)
}
