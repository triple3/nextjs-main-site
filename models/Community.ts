import { Model, model, Schema } from "mongoose";

export interface CommunityDbModel {
  _id?: string;
  question: string;
  active: boolean;
  count: number;
}
export const CommunityDbModelSchema = new Schema<CommunityDbModel>({
  question: { type: String, required: true },
  active: { type: Boolean, required: false },
  count: { type: Number, required: false, default: 0 },
});
