import { Model, model, Schema } from "mongoose";

export interface FaqDbModel {
  _id?: string;
  question: string;
  answer: string;
  active: boolean;
}
export const FaqDbModelSchema = new Schema<FaqDbModel>({
  question: { type: String, required: true },
  answer: { type: String, required: false },
  active: { type: Boolean, required: false },
});
