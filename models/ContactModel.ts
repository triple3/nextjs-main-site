import { Model, model, Schema } from "mongoose";

export interface ContactDbModel {
  _id?: string;
  firstName: string;
  lastName: string;
  email: string;
  socialMedia: string;
}
export const ContactDbModelSchema = new Schema<ContactDbModel>({
  firstName: { type: String, required: true },
  lastName: { type: String, required: false },
});
