import { model, connect, Model, models } from "mongoose";
import { FaqDbModel, FaqDbModelSchema } from "../models/FaqModel";
import { ContactDbModel, ContactDbModelSchema } from "../models/ContactModel";
import {
  CommunityDbModel,
  CommunityDbModelSchema,
} from "../models/Community";

let uri = process.env.MONGODB_URI;
let dbName = process.env.MONGODB_DB;
console.info("connect", uri, dbName);

if (!uri) {
  throw new Error(
    "Please define the MONGODB_URI environment variable inside .env.local"
  );
}

if (!dbName) {
  throw new Error(
    "Please define the MONGODB_DB environment variable inside .env.local"
  );
}

export async function connectToDatabase() {
  return await connect(`${uri}${dbName}`);
}

export const FaqModel: Model<FaqDbModel> =
  models.faq || model("faq", FaqDbModelSchema);
export const ContactModel: Model<ContactDbModel> =
  models.contact || model("contact", ContactDbModelSchema);
export const CommunityModel: Model<CommunityDbModel> =
  models.community || model("community", CommunityDbModelSchema);
