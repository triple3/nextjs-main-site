import Image from "next/image";
import Link from "next/link";
interface LogoProps { }
export const Logo = () => {
  return (
    <div className="heading">

      <div className="menuBar">
        <Link href="/">
          <a className="menuBar">Home</a>
        </Link>
        <Link href="music">
          <a className="menuBar">Music</a>
        </Link>
        <Link href="Store">
          <a className="menuBar">Store</a>
        </Link>
        <Link href="333community">
          <a className="menuBar">333 Community</a>
        </Link>
        <Link href="Submissions">
          <a className="menuBar">Submissions</a>
        </Link>
        <Link href="faq">
          <a className="menuBar">FAQ</a>
        </Link>
      </div>
      <div className="bg"></div>
      <div className="logo-centered">
        <img
          className="logo"
          src="/images/Artboard.png"
          alt="artboard"
          width="150px"
          height="138px"
        />
      </div>
      <h1>3RD HARMONIC RECORDS</h1>

    </div>
  );
};
