import Head from "next/head";
import { Logo } from "./Header";

export const siteTitle = "Next.js Sample Website";

export const Layout = ({ children }) => {
  return (
    <div className="outer">
      <div className="container">
        <Head>
          <link rel="icon" href="/favicon.ico" />
          <meta
            name="description"
            content="Learn how to build a personal website using Next.js"
          />
        </Head>
        <Logo />

        <main>{children}</main>
      </div>
    </div>
  );
};
