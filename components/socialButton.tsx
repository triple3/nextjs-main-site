import Image from 'next/image';

import instagram from '../public/images/instagram.svg';
import twitter from '../public/images/twitter.svg';
import styles from './socialButton.module.scss';
type SocialNetworkType = "instagram" | "twitter"
interface SocialButtonProps {
    href: string;
    socialNetworkType: SocialNetworkType;
    text: string;
}
const socialNetworkSrc = (val: SocialNetworkType) => {
    switch (val) {
        case "instagram":
            return instagram;
        case "twitter":
            return twitter;
    }
}
export const SocialButton = ({ href, socialNetworkType, text }: SocialButtonProps) => {
    return <a className={styles.button} href={href} target="blank">
        <Image src={socialNetworkSrc(socialNetworkType)} alt={socialNetworkType} width="24px" height="24px" />
        <span className={styles.padText}>{text}</span>
    </a>
}