import React, { useState } from "react";
import { FormikErrors, useFormik } from "formik";
import { FaqDbModel } from "../models/FaqModel";

interface FaqProps {
  answerElement: FaqDbModel;
  onDelete: () => void;
}
export const Faq = ({ answerElement, onDelete }: FaqProps) => {
  const [message, setMessage] = useState("");
  const formik = useFormik<FaqDbModel>({
    initialValues: {
      question: answerElement.question,
      answer: answerElement.answer,
      _id: answerElement._id,
      active: answerElement.active,
    },
    enableReinitialize: true,

    onSubmit: async (values) => {
      const result = await fetch("api/community/update", {
        headers: {
          "Content-Type": "application/json",
        },
        method: "POST",
        body: JSON.stringify(values),
      });
      const response = await result.json();
      setMessage(response.message);
    },
    validate: (values) => {
      console.log(values);
      const errors = {} as FormikErrors<FaqDbModel>;
      if (values.question.length > 500) {
        errors.question = "Too long";
      }
      return errors;
    },
  });
  async function del() {
    const result = await fetch("api/community/delete", {
      headers: {
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({ _id: answerElement._id, action: "delete" }),
    });
    const response = await result.json();
    setMessage(response.message);
    onDelete();
  }

  return (
    <form onSubmit={formik.handleSubmit}>
      <div>
        <div>
          <label htmlFor="question">Question</label>
          <button type="button" onClick={() => del()}>
            delete
          </button>
        </div>
        <input
          id="question"
          name="question"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.question}
        />
        {formik.errors.question}
      </div>

      <div>
        <div>
          <label htmlFor="answer">Answer</label>
        </div>
        <input
          id="answer"
          name="answer"
          type="text"
          onChange={formik.handleChange}
          value={formik.values.answer}
        />
      </div>
      <div>
        <label htmlFor="active">
          Active{" "}
          <input
            type="checkbox"
            id="active"
            name="active"
            onChange={formik.handleChange}
            value={formik.values.active as any}
          />
        </label>
      </div>
      <div className="onSubmit">
        <button type="submit">Submit</button>
        {message}
      </div>
    </form>
  );
};
